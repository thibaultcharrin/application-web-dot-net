#!/bin/bash

#VARIABLES
IMAGE=thibaultcharrin/dld-store
TAG=latest

#ARGS
if [[ $1 != "" ]]
then 
    TAG=$1
fi

#STEPS
echo -e "\n*** Building App ***"
if [ -d out ]
then
    echo "Using existing Binaries"
    if [ $(docker images | grep $IMAGE | wc -l) == 0 ]
    then
        echo "Creating image from current Binaries"
	    docker build -t "$IMAGE:$TAG" .
    else
        echo "Using current Docker image"
    fi
else
    dotnet clean
    dotnet restore
    dotnet publish -c Release -o out
    echo -e "\n*** Building Image ***"
    if [ $(docker images | grep $IMAGE | wc -l) == 1 ]
    then
        echo "Removing current image"
	    docker rmi -f $IMAGE:$TAG
    else
        echo "Creating image from new Binaries"
    fi
    docker build -t "$IMAGE:$TAG" .
    if [ $TAG != "latest" ]
    then
        docker build -t $IMAGE:latest .
    fi
fi
echo -e "\n*** Deploying App ***"
docker compose up -d