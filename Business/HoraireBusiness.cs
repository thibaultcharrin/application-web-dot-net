﻿using Fr.EQL.AI110.DLD_GC.DataAccess;
using Fr.EQL.AI110.DLD_GC.Entities;

namespace Fr.EQL.AI110.DLD_GC.Business
{
    public class HoraireBusiness
    {
        public void CreateHoraire(Horaire horaire)
        {
            HoraireDao dao = new HoraireDao();
            dao.Insert(horaire);
        }
    }
}