﻿using Fr.EQL.AI110.DLD_GC.DataAccess;
using Fr.EQL.AI110.DLD_GC.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Business
{
    public class StockageBusiness
    {
        public List<StockageDetails> GetAllStockage()
        {
           StockageDao dao = new StockageDao();
           return dao.GetAllStockage();
        }
        public List<Relais> GetRelais()
        {
            StockageDao dao = new StockageDao();
            return dao.GetAllRelais();
        }
    }
}
