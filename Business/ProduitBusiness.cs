﻿using Fr.EQL.AI110.DLD_GC.DataAccess;
using Fr.EQL.AI110.DLD_GC.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Business
{
    public class ProduitBusiness
    {        
        //Get All
        public List<Produit> GetProduits() 
        { 
            ProduitDao dao = new ProduitDao();
            return dao.GetAll();
        }
        public List<ProduitDetails> GetProduitDetails()
        {
            ProduitDao dao = new ProduitDao();
            return dao.GetAllDetails();
        }

        //Get By Id
        public ProduitDetails GetProduit(int id)
        {
            ProduitDao dao = new ProduitDao();
            return dao.GetById(id);
        }

        //Get by Parent
        public List<ProduitDetails> GetProduitbyParentId(int id)
        {
            ProduitDao dao = new ProduitDao();
            return dao.GetProduitByCat3Id(id);
        }
    }
}
