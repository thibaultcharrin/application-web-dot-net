﻿using Fr.EQL.AI110.DLD_GC.Entities;
using Fr.EQL.AI110.DLD_GC.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Business
{
    public class DonBusiness
    {

        // Confirmation Deposit In A Storage Space
        public void ConfirmationDepositInAStorageSpaceDon(DonDetails don)
        {
            DonDAO dao = new DonDAO();
            dao.ConfirmationDepositInAStorageSpace(don);
        }

        // Confirmation Echange Donor
        public void ConfirmationEchangeDonorDon(DonDetails don)
        {
            DonDAO dao = new DonDAO();
            dao.ConfirmationEchangeDonor(don);
        }

        // Confirmation Echange Beneficiary
        public void ConfirmationEchangeBeneficiaryDon(DonDetails don)
        {
            DonDAO dao = new DonDAO();
            dao.ConfirmationEchangeBeneficiary(don);
        }
        public void CancelReservationDon(DonDetails don)
        {
            DonDAO dao = new DonDAO();
            if (!don.Id.Equals(null))
            {
                dao.CancelReservation(don);
            }
        }

        // MAKE A RESERVATION
        public void MakeAReservationDon(DonDetails don)
        {
            DonDAO dao = new DonDAO();
            if (!don.Id.Equals(null))
            {
                dao.MakeAReservation(don);
            }
        }


        // UPDATE
        public void UpdateDon(DonDetails don)
        {
            
            DonDAO dao = new DonDAO();
            if (!don.Id.Equals(null))
            {
                dao.Update(don);
            }
        }
        
        // Cancel Don
        public void CancelDonDon(int id)
        {
            DonDAO dao = new DonDAO();
            dao.CancelDon(id);
        }
        
        #region CREATE
        public void CreateDon(Don don)
        {
            DonDAO dao = new DonDAO();
            dao.Insert(don);
        }

        public List<DonDetails>? SearchByDonor(int? selectedAdherentId)
        {
            DonDAO dao = new DonDAO();
            DonMultiSearch donMultiSearch = new DonMultiSearch();
            donMultiSearch.DonorId= selectedAdherentId;
            return dao.GetByMultiCriteria(donMultiSearch); 
        }

        public List<DonDetails>? SearchByBeneficiary(int? selectedAdherentId)
        {
            DonDAO dao = new DonDAO();
            DonMultiSearch donMultiSearch= new DonMultiSearch();
            donMultiSearch.BeneficiaryId= selectedAdherentId;
            return dao.GetByMultiCriteria(donMultiSearch);

        }
        #endregion

        #region READ

        //GET ALL
        public List<Don> GetDons()
        {
            DonDAO dao = new DonDAO();
            return dao.GetAll();
        }
        //GET ID DON Details
        public DonDetails GetDonDetailsById(int id)
        {
            DonDAO dao = new DonDAO();
            return dao.GetDonDetailsById(id);
        }

        //GET ID
        public Don GetDonById(int id)
        {
            DonDAO dao = new DonDAO();
            return dao.GetById(id);
        }

        //GET MULTI
        public List<DonDetails> SearchDons(DonMultiSearch donMultiSearch)
        {
            DonDAO dao = new DonDAO();
            return dao.GetByMultiCriteria(donMultiSearch);
        }

        public List<DonDetails> SearchByImplicatedAdherentId(int adherentId)
        {
            DonDAO dao = new DonDAO();
            return dao.GetByAdherent(adherentId);
        }

        //GET HORAIRES
        public List<Horaire> GetDonHorairesByDonId(int id)
        {
            DonDAO dao = new DonDAO();
            return dao.GetDonHoraires(id); 
        }

        public List<Horaire> GetStockageHorairesByStockageId(int id)
        {
            DonDAO dao = new DonDAO();
            return dao.GetStockageHoraires(id);
        }

        public void MakeAReservationWithBeneficiaryId(int? id, int? selectedAdherentId)
        {
            DonDAO dao = new DonDAO();
            if (!id.Equals(null))
            {
                dao.MakeAReservationWithBeneficiaryId(id,selectedAdherentId);
            }
        }
        #endregion


    }
}
