﻿using Fr.EQL.AI110.DLD_GC.DataAccess;
using Fr.EQL.AI110.DLD_GC.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Business
{
    public class CategorieBusiness
    {
        //Get All
        public List<Categorie> GetCategories()
        {
            CategorieDao dao = new CategorieDao();
            return dao.GetAllCategories();
        }
        public List<Categorie2> GetCategories2()
        {
            CategorieDao dao = new CategorieDao();
            return dao.GetAllCategories2();
        }
        public List<Categorie3> GetCategories3()
        {
            CategorieDao dao = new CategorieDao();
            return dao.GetAllCategories3();
        }

        //Get By Id
        public Categorie GetCategoryById(int id)
        {
            CategorieDao dao = new CategorieDao();
            return dao.GetCatById(id);
        }
        public Categorie2 GetCategory2ById(int id)
        {
            CategorieDao dao = new CategorieDao();
            return dao.GetCat2ById(id);
        }
        public Categorie3 GetCategory3ById(int id)
        {
            CategorieDao dao = new CategorieDao();
            return dao.GetCat3ById(id);
        }

        //Get by Parents
        public List<Categorie2> GetCat2byParentId(int id)
        {
            CategorieDao dao = new CategorieDao();
            return dao.GetCat2ByCatId(id);
        }
        public List<Categorie3> GetCat3byParentId(int id)
        {
            CategorieDao dao = new CategorieDao();
            return dao.GetCat3ByCat2Id(id);
        }
    }
}
