﻿using Fr.EQL.AI110.DLD_GC.DataAccess;
using Fr.EQL.AI110.DLD_GC.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Business
{
    public class JourBusiness
    {
        public List<Jour> GetAll()
        {
            JourDao dao = new JourDao();
            return dao.GetAllDays();

        }
    }
}
