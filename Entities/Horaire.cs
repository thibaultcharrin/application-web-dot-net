﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Horaire : Jour
    {
        //Règles de gestion

        //les régions balisées par des # sont des 'directives' (pas compilées)
        #region ATTRIBUTES  
        public int? Id { get; set; }
        public TimeSpan? MorningStart { get; set; }
        public TimeSpan? MorningEnd { get; set; }
        public TimeSpan? AfternoonStart { get; set; }
        public TimeSpan? AfternoonEnd { get; set; }
        public String? Day { get; set; }
        public int? JourId { get; set; }
        public int? DonId { get; set; }
        public int? StockageId { get; set; }

        #endregion

        #region CONSTRUCTORS
        public Horaire()
        {
        }

        public Horaire(int? id, TimeSpan? morningStart, TimeSpan? morningEnd, 
            TimeSpan? afternoonStart, TimeSpan? afternoonEnd, string? day)
        {
            Id = id;
            MorningStart = morningStart;
            MorningEnd = morningEnd;
            AfternoonStart = afternoonStart;
            AfternoonEnd = afternoonEnd;
            Day = day;
        }


        #endregion
    }
}
