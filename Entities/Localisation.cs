﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Localisation
    {
        public int? Id { get; set; }
        public string? City { get; set; }
        public string? PostCode { get; set; }

        public Localisation()
        {
        }

        public Localisation(int? id, string? city, string? postCode)
        {
            Id = id;
            City = city;
            PostCode = postCode;
        }
    }
}
