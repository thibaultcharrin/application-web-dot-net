﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Stockage : Relais
    {
        //Règles de gestion

        //les régions balisées par des # sont des 'directives' (pas compilées)
        #region ATTRIBUTES  
        public int? Id { get; set; }
        public int? AccessibiliteId { get; set; }
        public int? ConservationId { get; set; }
        public int? RelaisId { get; set; }
        public DateTime? CreateSpace { get; set; }
        public DateTime? RemoveSpace { get; set; }
        public string? RelaisName { get; set; }

        #endregion

        #region CONSTRUCTORS
        public Stockage()
        {
        }

        public Stockage(int? id, int? accessibiliteId, int? conservationId, 
            int? relaisId, DateTime? createSpace, DateTime? removeSpace, string? relaisName)
        {
            Id = id;
            AccessibiliteId = accessibiliteId;
            ConservationId = conservationId;
            RelaisId = relaisId;
            CreateSpace = createSpace;
            RemoveSpace = removeSpace;
            RelaisName = relaisName;
        }


        #endregion
    }
}
