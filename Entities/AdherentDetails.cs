﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class AdherentDetails : Adherent
    {
        #region ATTRIBUTES
        public int? DonId { get; set; }
        public int? BeneficiaryId { get; set; }
        public int? DonorId { get; set; }
        public int? IndisponibiliteId { get; set; }
        public int? AchatId { get; set; }
        public string? City { get; set; }
        public string? PostCode { get; set; }


        #endregion

        #region CONSTRUCTORS
        public AdherentDetails() { }
        public AdherentDetails(Adherent adherent)
        {
            LocalisationId = adherent.LocalisationId;
            FirstName = adherent.FirstName;
            LastName = adherent.LastName;
            Dob = adherent.Dob;
            Email = adherent.Email;
            Pw = adherent.Pw;
            StreetNb = adherent.StreetNb;
            StreetName = adherent.StreetName;
            CompAdr = adherent.CompAdr;
            Tel = adherent.Tel;
            CreateDate = adherent.CreateDate;
            DeactivateDate = adherent.DeactivateDate;
            ActivationCode = adherent.ActivationCode;
        }
        #endregion
    }
}
