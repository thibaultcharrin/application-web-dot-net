﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Categorie2
    {
        public int? Id { get; set; }
        public int? CatId { get; set; }
        public string? Name { get; set; }

        public Categorie2()
        {
        }

        public Categorie2(int? id, int? catId, string? name)
        {
            Id = id;
            CatId = catId;
            Name = name;
        }
    }
}
