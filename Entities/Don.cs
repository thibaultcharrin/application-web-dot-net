﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Don
    {
        //Règles de gestion

        //les régions balisées par des # sont des 'directives' (pas compilées)
        #region ATTRIBUTES  
        public int? Id { get; set; }
        public int? Qty { get; set; }
        public DateTime? Dlc { get; set; }
        public DateTime? Ddm { get; set; }
        public string? Picture { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ReserveDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public int? AltStreetNb { get; set; }
        public string? AltStreetName { get; set; }
        public string? AltCompAdr { get; set; }
        public DateTime? AvailableCollect { get; set; }
        public DateTime? Receive { get; set; }
        public DateTime? NoReceive { get; set; }
        public DateTime? Handover { get; set; }
        public DateTime? NoHandover { get; set; }
        public DateTime? FinalDate { get; set; }
        public int? BeneficiaryId { get; set; }
        public int? StockageId { get; set; }
        public int? ProduitId { get; set; }
        public int? DonorId { get; set; }
        public int? UniteId { get; set; }
        public int? ConservationId { get; set; }
        public int? LocalisationId { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Don()
        {
        }

        public Don(
            int? id, int? qty, DateTime? dlc, DateTime? ddm, string? picture, DateTime? createDate,
            DateTime? reserveDate, DateTime? cancelDate, int? altStreetNb, string? altStreetName,
            string? altCompAdr, DateTime? availableCollect, DateTime? receive, DateTime? noReceive,
            DateTime? handover, DateTime? noHandover, DateTime? finalDate, int? beneficiaryId,
            int? stockageId, int? produitId, int? donorId, int? uniteId, int? conservationId,
            int? localisationId
            )
        {
            Id = id;
            Qty = qty;
            Dlc = dlc;
            Ddm = ddm;
            Picture = picture;
            CreateDate = createDate;
            ReserveDate = reserveDate;
            CancelDate = cancelDate;
            AltStreetNb = altStreetNb;
            AltStreetName = altStreetName;
            AltCompAdr = altCompAdr;
            AvailableCollect = availableCollect;
            Receive = receive;
            NoReceive = noReceive;
            Handover = handover;
            NoHandover = noHandover;
            FinalDate = finalDate;
            BeneficiaryId = beneficiaryId;
            StockageId = stockageId;
            ProduitId = produitId;
            DonorId = donorId;
            UniteId = uniteId;
            ConservationId = conservationId;
            LocalisationId = localisationId;
        }
        #endregion
    }
}
