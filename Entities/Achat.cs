﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Achat
    {
        public int Id { get; set; }
        public int AdherentId { get; set; }
        public int CarnetId { get; set; }
        public int Quantity { get; set; }
        public DateTime date { get; set; }

        public Achat(int id, int adherentId, int carnetId, int quantity, DateTime date)
        {
            Id = id;
            AdherentId = adherentId;
            CarnetId = carnetId;
            Quantity = quantity;
            this.date = date;
        }
        public Achat()
        {

        }
    }
}
