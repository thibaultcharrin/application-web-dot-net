﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class ProduitDetails : Produit
    {
        #region ATTRIBUTES
        public string? Cat3Name { get; set; }
        public int? Cat2Id { get; set; }
        public string? Cat2Name { get; set; }
        public int? CatId { get; set; }
        public string? CatName { get; set; }
        public string? ConservationName { get; set; }
        public int? UniteId { get; set; }
        public string? UniteName { get; set; }
        #endregion

        #region CONSTRUCTORS
       public ProduitDetails()
        {
        }

        public ProduitDetails(Produit produit)
        {
            Id = produit.Id;
            Cat3Id = produit.Cat3Id;
            ConservationId = produit.ConservationId;
            Name = produit.Name;
            ConservationName = this.ConservationName;
        }
        #endregion

    }
}
