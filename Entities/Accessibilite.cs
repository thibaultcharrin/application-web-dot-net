﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Accessibilite
    {
        public int? Id { get; set; }
        public string? Name { get; set; }

        public Accessibilite()
        {
        }

        public Accessibilite(int? id, string? name)
        {
            Id = id;
            Name = name;
        }
    }
}
