﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Relais
    {
        //Règles de gestion

        //les régions balisées par des # sont des 'directives' (pas compilées)
        #region ATTRIBUTES  
        public int? RelaisId { get; set; }
        public int? LocalisationId { get; set; }
        public int? PartenaireId { get; set; }
        public string? RelaisName { get; set; }
        public int? StreetNumber { get; set; }
        public string? StreetName { get; set; }
        public string? CompAdr { get; set; }


        #endregion

        #region CONSTRUCTORS
        public Relais()
        {
        }

        public Relais(int? id, int? localisationId, int? partenaireId, 
            string? name, int? streetNumber, string? streetName, string? compAdr)
        {
            RelaisId = id;
            LocalisationId = localisationId;
            PartenaireId = partenaireId;
            RelaisName = name;
            StreetNumber = streetNumber;
            StreetName = streetName;
            CompAdr = compAdr;
        }


        #endregion
    }
}
