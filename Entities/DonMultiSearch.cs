﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class DonMultiSearch : Don
    {
        #region ATTRIBUTES
        public DateTime? DlcMin { get; set; }
        public DateTime? DlcMax { get; set; }
        public DateTime? DdmMin { get; set; }
        public DateTime? DdmMax { get; set; }
        public DateTime? CreateDateMin { get; set; }
        public DateTime? CreateDateMax { get; set; }
        public DateTime? ReserveDateMin { get; set; }
        public DateTime? ReserveDateMax { get; set; }
        public DateTime? CancelDateMin { get; set; }
        public DateTime? CancelDateMax { get; set; }
        public DateTime? AvailableCollectMin { get; set; }
        public DateTime? AvailableCollectMax { get; set; }
        public DateTime? ReceiveMin { get; set; }
        public DateTime? ReceiveMax { get; set; }
        public DateTime? NoReceiveMin { get; set; }
        public DateTime? NoReceiveMax { get; set; }
        public DateTime? HandoverMin { get; set; }
        public DateTime? HandoverMax { get; set; }
        public DateTime? NoHandoverMin { get; set; }
        public DateTime? NoHandoverMax { get; set; }
        public DateTime? FinalDateMin { get; set; }
        public DateTime? FinalDateMax { get; set; }

        public string? ProduitName { get; set; }

        public int? CatId { get; set; }
        public int? Cat2Id { get; set; }
        public int? Cat3Id { get; set; }
        #endregion

        #region CONSTRUCTORS
        public DonMultiSearch()
        {
        }

        public DonMultiSearch(DateTime? dlcMin, DateTime? dlcMax, DateTime? ddmMin, DateTime? ddmMax,
            DateTime? createDateMin, DateTime? createDateMax, DateTime? reserveDateMin, DateTime? reserveDateMax,
            DateTime? cancelDateMin, DateTime? cancelDateMax, DateTime? availableCollectMin, DateTime? availableCollectMax,
            DateTime? receiveMin, DateTime? receiveMax, DateTime? noReceiveMin, DateTime? noReceiveMax, DateTime? handoverMin,
            DateTime? handoverMax, DateTime? noHandoverMin, DateTime? noHandoverMax, DateTime? finalDateMin, DateTime? finalDateMax,
            int? id, int? qty, DateTime? dlc, DateTime? ddm, string? picture, DateTime? createDate,
            DateTime? reserveDate, DateTime? cancelDate, int? altStreetNb, string? altStreetName,
            string? altCompAdr, DateTime? availableCollect, DateTime? receive, DateTime? noReceive,
            DateTime? handover, DateTime? noHandover, DateTime? finalDate, int? beneficiaryId,
            int? stockageId, int? produitId, int? donorId, int? uniteId, int? conservationId,
            int? localisationId, int? catId, int? cat2Id, int? cat3Id)

        {
            DlcMin = dlcMin;
            DlcMax = dlcMax;
            DdmMin = ddmMin;
            DdmMax = ddmMax;
            CreateDateMin = createDateMin;
            CreateDateMax = createDateMax;
            ReserveDateMin = reserveDateMin;
            ReserveDateMax = reserveDateMax;
            CancelDateMin = cancelDateMin;
            CancelDateMax = cancelDateMax;
            AvailableCollectMin = availableCollectMin;
            AvailableCollectMax = availableCollectMax;
            ReceiveMin = receiveMin;
            ReceiveMax = receiveMax;
            NoReceiveMin = noReceiveMin;
            NoReceiveMax = noReceiveMax;
            HandoverMin = handoverMin;
            HandoverMax = handoverMax;
            NoHandoverMin = noHandoverMin;
            NoHandoverMax = noHandoverMax;
            FinalDateMin = finalDateMin;
            FinalDateMax = finalDateMax;

            Id = id;
            Qty = qty;
            Dlc = dlc;
            Ddm = ddm;
            Picture = picture;
            CreateDate = createDate;
            ReserveDate = reserveDate;
            CancelDate = cancelDate;
            AltStreetNb = altStreetNb;
            AltStreetName = altStreetName;
            AltCompAdr = altCompAdr;
            AvailableCollect = availableCollect;
            Receive = receive;
            NoReceive = noReceive;
            Handover = handover;
            NoHandover = noHandover;
            FinalDate = finalDate;
            BeneficiaryId = beneficiaryId;
            StockageId = stockageId;
            ProduitId = produitId;
            DonorId = donorId;
            UniteId = uniteId;
            ConservationId = conservationId;
            LocalisationId = localisationId;

            CatId = catId;
            Cat2Id = cat2Id;
            Cat3Id = cat3Id;
        }
        #endregion
    }
}
