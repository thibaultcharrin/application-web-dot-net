﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Categorie3
    {
        public int? Id { get; set; }
        public int? Cat2Id { get; set; }
        public string? Name { get; set; }

        public Categorie3()
        {
        }

        public Categorie3(int? id, int? cat2Id, string? name)
        {
            Id = id;
            Cat2Id = cat2Id;
            Name = name;
        }
    }
}
