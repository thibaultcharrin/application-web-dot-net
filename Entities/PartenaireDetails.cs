﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class PartenaireDetails : Partenaire
    {
        #region ATTRIBUTES
        public string PointRelais { get; set; }
        //FAIRE EVOLUER LES ATTRIBUTS EN FONCTION DES BESOINS
        #endregion

        #region CONSTRUCTORS
        public PartenaireDetails(Partenaire partenaire)
        {
            Id = partenaire.Id;
            LocalisationId = partenaire.LocalisationId;
            Name = partenaire.Name;
            Email = partenaire.Email;
            Pw = partenaire.Pw;
            Tel = partenaire.Tel;
            StreetNb = partenaire.StreetNb;
            StreetName = partenaire.StreetName;
            CompAdr = partenaire.CompAdr;
            RegisterDate = partenaire.RegisterDate;
            ValidateDate = partenaire.ValidateDate;
            DeactivateDate = partenaire.DeactivateDate;
            ActivationCode = partenaire.ActivationCode;
        }
        #endregion
    }
}
