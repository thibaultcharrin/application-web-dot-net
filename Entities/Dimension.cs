﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class Dimension
    {
        public int? Id { get; set; }
        public int? StockageId { get; set; }
        public string? Name { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
        public int? Depth { get; set; }

        public Dimension()
        {
        }

        public Dimension(int? id, int? stockageId, string? name, int? height, int? width, int? depth)
        {
            Id = id;
            StockageId = stockageId;
            Name = name;
            Height = height;
            Width = width;
            Depth = depth;
        }
    }
}
