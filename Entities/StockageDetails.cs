﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.Entities
{
    public class StockageDetails : Stockage
    {
        public int? DimensionId { get; set; }
        public int? DonId { get; set; }
        public int? IndisponibiliteId { get; set; }

        public StockageDetails()
        {
        }

        public StockageDetails(Stockage sto)
        {
            RelaisName = sto.RelaisName;
            Id = sto.Id;
            AccessibiliteId = sto.AccessibiliteId;
            ConservationId = sto.ConservationId;
            RelaisId = sto.RelaisId;
            CreateSpace = sto.CreateSpace;
            RemoveSpace = sto.RemoveSpace;
        }
    }
}
