﻿using Fr.EQL.AI110.DLD_GC.Entities;
using Fr.EQL.AI110.DLD_GC.Business;
using Fr.EQL.AI110.DLD_GC.WebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.DLD_GC.WebApp.Controllers
{
    public class CatalogController : Controller
    {
        //ACCUEIL
        public IActionResult Main()
        {
            return View();
        }

        //RECHERCHE
        [HttpGet]
        public IActionResult FeL(string rayonSelector, int rayonId)
        {
            // récupérer le modele
            DonSearchViewModel model = new DonSearchViewModel();
            CategorieBusiness catBU = new CategorieBusiness();
            ProduitBusiness proBU = new ProduitBusiness();
            DonBusiness donBU = new DonBusiness();

            model.RayonSelector = rayonSelector;
            model.Rayon = rayonId;

            if (model.Rayon != 0)
            {
                model.SearchCriteria.CatId = model.Rayon;
            }
            else
            {
                model.SearchCriteria.CatId = null;
            }
            if (model.Categorie != 0)
            {
                model.SearchCriteria.Cat2Id = model.Categorie;
            }
            else
            {
                model.SearchCriteria.Cat2Id = null;
            }
            if (model.SousCategorie != 0)
            {
                model.SearchCriteria.Cat3Id = model.SousCategorie;
            }
            else
            {
                model.SearchCriteria.Cat3Id = null;
            }

            model.Categories2 = catBU.GetCat2byParentId(model.Rayon);
            model.Categories3 = catBU.GetCat3byParentId(model.Categorie);
            model.Produits = proBU.GetProduitbyParentId(model.SousCategorie);
            //model.Produits = proBU.GetProduitDetails();

            model.Dons = donBU.SearchDons(model.SearchCriteria);

            // charger la vue :
            return View(model);
        }

        [HttpPost]
        public IActionResult FeL(DonSearchViewModel model)
        {
            CategorieBusiness catBU = new CategorieBusiness();
            ProduitBusiness proBU = new ProduitBusiness();
            DonBusiness donBU = new DonBusiness();

            if (model.Rayon != 0)
            {
                model.SearchCriteria.CatId = model.Rayon;
            }
            else
            {
                model.SearchCriteria.CatId = null;
            }
            if (model.Categorie != 0)
            {
                model.SearchCriteria.Cat2Id = model.Categorie;
            }
            else
            {
                model.SearchCriteria.Cat2Id = null;
            }
            if (model.SousCategorie != 0)
            {
                model.SearchCriteria.Cat3Id = model.SousCategorie;
            }
            else
            {
                model.SearchCriteria.Cat3Id = null;
            }

            model.Categories2 = catBU.GetCat2byParentId(model.Rayon);
            model.Categories3 = catBU.GetCat3byParentId(model.Categorie);
            model.Produits = proBU.GetProduitbyParentId(model.SousCategorie);
            
            model.Dons = donBU.SearchDons(model.SearchCriteria);

            return View(model);
        }

        //MES DONS
        [HttpGet]
        public IActionResult MesDons()
        {
            MesDonsViewModel model = new MesDonsViewModel();
            AdherentBusiness adhBus = new AdherentBusiness();
            DonBusiness donBus = new DonBusiness();
            model.Adherents = adhBus.GetAllAdherentDetails();
            DonMultiSearch donMultiSearch = new DonMultiSearch();
            model.Dons = donBus.SearchDons(donMultiSearch);
            model.AdherentRole = 0;
            model.SelectedAdherentId = 0;
            return View(model);
        }

        [HttpPost]
        public IActionResult MesDons(MesDonsViewModel model)
        {
            AdherentBusiness adhBus = new AdherentBusiness();
            DonBusiness donBus = new DonBusiness();

            model.Adherents = adhBus.GetAllAdherentDetails();
            DonMultiSearch donMultiSearch = new DonMultiSearch();
            if (model.SelectedAdherentId == 0)
            {
                model.Dons = donBus.SearchDons(donMultiSearch);
                model.AdherentRole = 0;
                model.SelectedAdherentId = 0;
                return View(model);
            }
            if (model.AdherentRole == 0)
            {
                model.Dons = donBus.SearchByImplicatedAdherentId((int)model.SelectedAdherentId);
            }
            else if (model.AdherentRole == 1)
            {
                model.Dons = donBus.SearchByDonor(model.SelectedAdherentId);
            }
            else
            {
                model.Dons = donBus.SearchByBeneficiary(model.SelectedAdherentId);
            }
            return View(model);
        }

    }
}
