﻿using Fr.EQL.AI110.DLD_GC.Entities;
using Fr.EQL.AI110.DLD_GC.Business;
using Fr.EQL.AI110.DLD_GC.WebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.DLD_GC.WebApp.Controllers
{
    public class DonController : Controller
    {

        [HttpGet]
        public IActionResult ConfirmStockage(int donId)
        {
            DonBusiness dbu = new DonBusiness();
            DonDetails don = dbu.GetDonDetailsById(donId);
            dbu.ConfirmationDepositInAStorageSpaceDon(don);
            return View("Stockage", don);
        }

        [HttpPost]
        public IActionResult ConfirmStockage(DonDetails don)
        {
            DonDetailsViewModel model = new DonDetailsViewModel();

            if (ModelState.IsValid)
            {
                try
                {
                    DonBusiness dbu = new DonBusiness();
                    model.DonDetails = don;
                    model.SelectedAdherentId = don.BeneficiaryId;
                    return RedirectToAction("Details", new { donId = model.DonDetails.Id, adherentId = model.SelectedAdherentId });
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Stockage", don);
                }

            }
            else
            {
                return View("Stockage", don);
            }
        }

        [HttpGet]
        public IActionResult ConfirmHandover(int donId)
        {
            DonBusiness dbu = new DonBusiness();
            DonDetails don = dbu.GetDonDetailsById(donId);
            dbu.ConfirmationEchangeDonorDon(don);
            return View("Transfert", don);
        }

        [HttpPost]
        public IActionResult ConfirmHandover(DonDetails don)
        {
            DonDetailsViewModel model = new DonDetailsViewModel();

            if (ModelState.IsValid)
            {
                try
                {
                    DonBusiness dbu = new DonBusiness();
                    model.DonDetails = don;
                    model.SelectedAdherentId = don.BeneficiaryId;
                    return RedirectToAction("Details", new { donId = model.DonDetails.Id, adherentId = model.SelectedAdherentId });
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Transfert", don);
                }

            }
            else
            {
                return View("Transfert", don);
            }
        }

        [HttpGet]
        public IActionResult ConfirmReceived(int donId)
        {
            DonBusiness dbu = new DonBusiness();
            DonDetails don = dbu.GetDonDetailsById(donId);
            dbu.ConfirmationEchangeBeneficiaryDon(don);
            return View("Reception", don);
        }

        [HttpPost]
        public IActionResult ConfirmReceived(DonDetails don)
        {
            DonDetailsViewModel model = new DonDetailsViewModel();

            if (ModelState.IsValid)
            {
                try
                {
                    DonBusiness dbu = new DonBusiness();
                    model.DonDetails = don;
                    model.SelectedAdherentId = don.BeneficiaryId;
                    return RedirectToAction("Details", new { donId = model.DonDetails.Id, adherentId = model.SelectedAdherentId });
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Reception", don);
                }

            }
            else
            {
                return View("Reception", don);
            }
        }

        [HttpGet]
        public IActionResult CancelReservation(int donId)
        {
            DonBusiness dbu = new DonBusiness();
            DonDetails don = dbu.GetDonDetailsById(donId);
            dbu.CancelReservationDon(don);
            return View("CancelReserve", don);
        }

        [HttpPost]
        public IActionResult CancelReservation(DonDetails don)
        {
            DonDetailsViewModel model = new DonDetailsViewModel();

            if (ModelState.IsValid)
            {
                try
                {
                    DonBusiness dbu = new DonBusiness();
                    model.DonDetails = don;
                    model.SelectedAdherentId = don.BeneficiaryId;
                    return RedirectToAction("Details", new { donId = model.DonDetails.Id, adherentId = model.SelectedAdherentId });
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return View("CancelReserve", don);
                }

            }
            else
            {
                return View("CancelReserve", don);
            }
        }

        //[HttpGet]
        //public IActionResult MakeAReservation(int id)
        //{
        //    DonBusiness dbu = new DonBusiness();
        //    DonDetails don = dbu.GetDonDetailsById(id);

        //    return View ("Reserve", don);
        //}

        [HttpGet]
        public IActionResult MakeAReservation(int donId, int beneficiaryId)
        {
            DonBusiness dbu = new DonBusiness();
            dbu.MakeAReservationWithBeneficiaryId(donId, beneficiaryId);
            DonDetails don = dbu.GetDonDetailsById(donId);

            return View("Reserve", don);
        }
        
        [HttpPost]
        public IActionResult MakeAReservation(DonDetails don)
        {
            DonDetailsViewModel model = new DonDetailsViewModel();
            if (ModelState.IsValid)
            {
                try
                {
                    DonBusiness dbu = new DonBusiness();
                    model.DonDetails = don;
                    model.SelectedAdherentId = don.BeneficiaryId;
                    return RedirectToAction("Details",new {donId=model.DonDetails.Id,adherentId=model.DonDetails.BeneficiaryId});
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Reserve", don);
                }

            }
            else
            {
                return View("Reserve", don);
            }
        }
        
        /*
        private IActionResult EditerDon(string rubrique, Don don)
        {
            ViewBag.Rubrique = rubrique;
            return View("New", don);
        }
        */
        
        [HttpGet]
        public IActionResult Update(int id)
        {
            DonBusiness dbu = new DonBusiness();
            DonDetails don = dbu.GetDonDetailsById(id);

            return View("Maj", don);
        }

        [HttpPost]
        public IActionResult Update(DonDetails don)
        {
            if (1 == 1)
            {
                try
                {
                    DonBusiness dbu = new DonBusiness();
                    dbu.UpdateDon(don);
                    return RedirectToAction("Details", don);
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Maj", don);
                }

            }
            else
            {
                return View ("Maj", don);
            }
        }

        #region CREATE


        /*
        [HttpGet]
        public IActionResult Create()
        {
            return NEW(null);
        }
        [HttpPost]
        public IActionResult Create(DonDetails don)
        {
            if (ModelState.IsValid)
            {
            DonBusiness bu = new DonBusiness();
            bu.Insert(don);
            return RedirectToAction("Details");
        }
            else
            {
                // si pas valide, j'affiche à nouveau la vue :
                return New(don);
            }
        }
        */
        #endregion

        #region READ
        //RECHERCHE MULTICRITERE
        [HttpGet]
        public IActionResult Search()
        {
            // récupérer le modele
            DonSearchViewModel model = new DonSearchViewModel();

            CategorieBusiness catBU = new CategorieBusiness();
            ProduitBusiness proBU = new ProduitBusiness();
            model.Categories = catBU.GetCategories();
            model.Categories2 = catBU.GetCategories2();
            model.Categories3 = catBU.GetCategories3();

            model.Produits = proBU.GetProduitDetails();

            DonBusiness donBU = new DonBusiness();
            model.Dons = donBU.SearchDons(model.SearchCriteria);

            // charger la vue :
            return View(model);
        }

        [HttpPost]
        public IActionResult Search(DonSearchViewModel model)
        {
            CategorieBusiness catBU = new CategorieBusiness();
            ProduitBusiness proBU = new ProduitBusiness();
            model.Categories = catBU.GetCategories();
            model.Categories2 = catBU.GetCat2byParentId(model.Rayon);
            model.Categories3 = catBU.GetCat3byParentId(model.Categorie);
            model.Produits = proBU.GetProduitbyParentId(model.SousCategorie);

            DonBusiness donBU = new DonBusiness();
            model.Dons = donBU.SearchDons(model.SearchCriteria);

            return View(model);
        }

        [HttpGet]
        public IActionResult CancelDon(int donId, int beneficiaryId)
        {
            DonBusiness dbu = new DonBusiness();
            dbu.CancelDonDon(donId);
            DonDetails don = dbu.GetDonDetailsById(donId);

            return View("CancelDon", don);
        }

        [HttpPost]
        public IActionResult CancelDon(DonDetails don)
        {
            DonDetailsViewModel model = new DonDetailsViewModel();
            if (ModelState.IsValid)
            {
                try
                {
                    DonBusiness dbu = new DonBusiness();
                    model.DonDetails = don;
                    model.SelectedAdherentId = don.BeneficiaryId;
                    return RedirectToAction("Details", new { donId = model.DonDetails.Id, adherentId = model.DonDetails.BeneficiaryId });
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return View("CancelDon", don);
                }

            }
            else
            {
                return View("CancelDon", don);
            }
        }

        //GET ALL
        [HttpGet]
        public IActionResult New()
        {
            StockageBusiness stoBu = new StockageBusiness();
            List<StockageDetails> stockages = stoBu.GetAllStockage();
            List<Relais> relais = stoBu.GetRelais();
            ViewBag.ListRelais = relais;
            ViewBag.ListStockages = stockages;
            DonBusiness DonBu = new DonBusiness();
            DonBusiness donBu = new DonBusiness();
            ProduitBusiness proBu = new ProduitBusiness();
            List<ProduitDetails> produits = proBu.GetProduitDetails();
            ViewBag.ListProduitsDetails = produits;
            JourBusiness jourBu = new JourBusiness();
            List<Jour> jours = jourBu.GetAll();
            ViewBag.ListJours = jours;
            AdherentBusiness adBu = new AdherentBusiness();
            List<AdherentDetails> adherents = adBu.GetAllAdherentDetails();
            ViewBag.ListAdherents = adherents;
            CategorieBusiness catBu = new CategorieBusiness();
            List<Categorie> categories = catBu.GetCategories();
            ViewBag.ListCategories = categories;
            List<Categorie2> categories2 = catBu.GetCategories2();
            ViewBag.ListCategories2 = categories2;
            List<Categorie3> categories3 = catBu.GetCategories3();
            ViewBag.ListCategories3 = categories3;
            UniteBusiness uniteBu = new UniteBusiness();
            List<Unite> unites = uniteBu.GetUnites();
            ViewBag.ListUnites = unites;
            ConservationBusiness conservationBu = new ConservationBusiness();
            List<Conservation> conservations = conservationBu.GetConservations();
            ViewBag.ListConservations = conservations;
            return View("New");
        }
        [HttpPost]
        public IActionResult New(DonDetails don)
        {

            DonBusiness donBu = new DonBusiness();
            donBu.CreateDon(don);
            HoraireBusiness horBu = new HoraireBusiness();
            //for (int i = 0; i<7; i++)
            //{
            //    don.HorairesDon[i].DonId = don.Id;
            //    don.HorairesDon[i].JourId = i+1;
            //    horBu.CreateHoraire(don.HorairesDon[i]);
            //}
            
            
            return RedirectToAction("MesDons", "Catalog");

           
        }

        //DETAILS
        //[HttpGet]
        //public IActionResult Details(int id)
        //{
        //    DonSearchViewModel model = new DonSearchViewModel();
        //    model.SearchCriteria.Id = id;


        //    DonBusiness donBU = new DonBusiness();
        //    List<DonDetails> dons = donBU.SearchDons(model.SearchCriteria);
        //    DonDetailsViewModel viewModel = new DonDetailsViewModel();
        //    viewModel.DonDetails = dons[0];
        //    viewModel.SelectedAdherentId = 0;
        //    AdherentBusiness adhBusiness = new AdherentBusiness();
        //    viewModel.Adherents = adhBusiness.GetAllAdherentDetails();

        //    return View(viewModel);
        //}

        [HttpGet]
        public IActionResult Details(int donId, int? adherentId)
        {
            DonSearchViewModel model = new DonSearchViewModel();
            model.SearchCriteria.Id = donId;


            DonBusiness donBU = new DonBusiness();
            List<DonDetails> dons = donBU.SearchDons(model.SearchCriteria);
            DonDetailsViewModel viewModel = new DonDetailsViewModel();
            viewModel.DonDetails = dons[0];
            if (adherentId.HasValue)
            {
                viewModel.SelectedAdherentId = adherentId;
            }
            else
            {
                viewModel.SelectedAdherentId = 0;
            }
            AdherentBusiness adhBusiness = new AdherentBusiness();
            viewModel.Adherents = adhBusiness.GetAllAdherentDetails();

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Details(DonDetailsViewModel donDetailsViewModel)
        {
            DonSearchViewModel model = new DonSearchViewModel();
            model.SearchCriteria.Id = donDetailsViewModel.DonDetails.Id;


            DonBusiness donBU = new DonBusiness();
            List<DonDetails> dons = donBU.SearchDons(model.SearchCriteria);
            DonDetailsViewModel viewModel = new DonDetailsViewModel();
            viewModel.DonDetails = dons[0];
            viewModel.SelectedAdherentId = donDetailsViewModel.SelectedAdherentId;
            AdherentBusiness adhBusiness = new AdherentBusiness();
            viewModel.Adherents = adhBusiness.GetAllAdherentDetails();
            return View(viewModel);
        }
        #endregion
    }
}
