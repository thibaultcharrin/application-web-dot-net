﻿using Fr.EQL.AI110.DLD_GC.Entities;

namespace Fr.EQL.AI110.DLD_GC.WebApp.Models
{
    public class MesDonsViewModel
    {
        public List<DonDetails>? Dons { get; set; }
        public List<AdherentDetails>? Adherents { get; set; }
        public int? SelectedAdherentId { get; set; }
        public int? AdherentRole { get; set; } = 0;
    }
}
