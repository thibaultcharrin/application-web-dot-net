#!/bin/bash

#VARIABLES
IMAGE=thibaultcharrin/dld-store
TAG=latest
CONTAINER_REGISTRY=""

#ARGS
if [[ $1 != "" ]]
then 
    TAG=$1
fi
if [[ $2 != "" ]]
then 
    CONTAINER_REGISTRY=$2
fi

if [ -f ~/.docker/config.json ]
then
    if [ ! -d out ]
    then
        . ./startup.sh $TAG
    fi
    docker build -t $CONTAINER_REGISTRY$IMAGE:$TAG .
    docker push $CONTAINER_REGISTRY$IMAGE:$TAG
    if [ $TAG != "latest" ]
    then
        docker build -t $CONTAINER_REGISTRY$IMAGE:latest .
        docker push $CONTAINER_REGISTRY$IMAGE:latest
    fi        
else
    echo "please check your .docker/config.json "
    exit
fi