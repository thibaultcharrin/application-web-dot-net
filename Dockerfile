# RESOURCES:
# https://docs.docker.com/samples/dotnetcore/

FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY out/ ./
ENTRYPOINT ["dotnet", "WebApp.dll"]
