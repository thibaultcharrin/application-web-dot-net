﻿using Fr.EQL.AI110.DLD_GC.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Fr.EQL.AI110.DLD_GC.DataAccess
{
    public class DonDAO : DAO
    {

        #region CREATE
        public void Insert(Don don)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO don
                            (produit_id, donor_id, unite_id, conservation_id, qty, 
                            dlc, ddm, picture, create_date)
                            VALUES
                            (@ProduitId, @DonorId, @UniteId, @ConservationId, 
                            @Quantity, @Dlc, @Ddm, @Picture, @CreateDate)";

            cmd.Parameters.Add(new MySqlParameter("ProduitId", don.ProduitId));
            cmd.Parameters.Add(new MySqlParameter("DonorId", don.DonorId));
            cmd.Parameters.Add(new MySqlParameter("UniteId", don.UniteId));  
            cmd.Parameters.Add(new MySqlParameter("ConservationId", don.ConservationId));
            cmd.Parameters.Add(new MySqlParameter("Quantity", don.Qty));
            cmd.Parameters.Add(new MySqlParameter("Dlc", don.Dlc));
            cmd.Parameters.Add(new MySqlParameter("Ddm", don.Ddm));
            cmd.Parameters.Add(new MySqlParameter("Picture", don.Picture));
            cmd.Parameters.Add(new MySqlParameter("CreateDate", DateTime.Now));
          

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la création du don " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        #endregion

        #region READ
        public List<Don> GetAll()
        {
            List<Don> result = new List<Don>();

            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT * FROM don";

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Don don = DataReaderToEntity(dr);

                    result.Add(don);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(
                    "Erreur lors de la récupération des dons : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public Don GetById(int id)
        {
            Don don = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM don WHERE id = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    don = DataReaderToDon(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return don;
        }

        public DonDetails GetDonDetailsById(int id)
        {

            DonDetails result = new DonDetails();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT  d.*,
                                        loc.city don_city, loc.postcode don_postcode,
                                        uni.name unite_name, 
                                        cons_don.name cons_don_name,

                                        donor.id donor_id,donor.firstname donor_firstname,donor.lastname donor_lastname,
                                        donor.email donor_email, donor.tel donor_tel, donor.street_nb donor_street_nb, donor.street_name donor_street_name, donor.comp_adr donor_comp_adr,
                                        donor_loc.city donor_city, donor_loc.postcode donor_postcode,

                                        beneficiary.id bene_id, beneficiary.firstname bene_firstname, beneficiary.lastname bene_lastname,
                                        beneficiary.email bene_email, beneficiary.tel bene_tel, beneficiary.street_nb bene_street_nb, beneficiary.street_name bene_street_name, beneficiary.comp_adr bene_comp_adr,
                                        bene_loc.city bene_city, bene_loc.postcode bene_postcode,

                                        p.name p_name,
                                        c.id c_id, c2.id c2_id, c3.id c3_id,
                                        c.name cat_name,c2.name cat2_name,c3.name cat3_name,
                                
                                        rel.street_nb rel_street_nb, rel.street_name rel_street_name, rel.comp_adr rel_comp_adr,
                                        loc_relais.city relais_city, loc_relais.postcode relais_postcode,

                                        part.street_nb part_street_nb, part.street_name part_street_name, part.comp_adr part_comp_adr,
                                        loc_part.city part_city, loc_part.postcode part_postcode,

                                        accs_stockage.name accs_stockage_name, cons_stockage.name cons_stockage_name,

                                        indispo_adherent.start indispo_adherent_start, indispo_adherent.end indispo_adherent_end,
                                        indispo_stockage.start indispo_stockage_start, indispo_stockage.end indispo_stockage_end

                                    FROM don d
                                        LEFT JOIN adherent donor ON d.donor_id=donor.id
                                        LEFT JOIN localisation donor_loc ON donor_loc.id=donor.localisation_id
                                        LEFT JOIN adherent beneficiary ON d.beneficiary_id=beneficiary.id
                                        LEFT JOIN localisation bene_loc ON bene_loc.id=beneficiary.localisation_id
                                        LEFT JOIN produit p ON d.produit_id=p.id
								        LEFT JOIN unite uni ON d.unite_id=uni.id
                                        LEFT JOIN stockage stock ON d.stockage_id=stock.id
                                        LEFT JOIN conservation cons_stockage ON stock.conservation_id=cons_stockage.id
                                        LEFT JOIN localisation loc ON d.localisation_id=loc.id
                                        LEFT JOIN cat3 c3 ON p.cat3_id=c3.id
                                        LEFT JOIN cat2 c2 ON c3.cat2_id=c2.id
                                        LEFT JOIN cat c ON c2.cat_id=c.id
                                        LEFT JOIN relais rel ON stock.relais_id=rel.id
                                        LEFT JOIN localisation loc_relais ON loc_relais.id=rel.localisation_id
                                        LEFT JOIN partenaire part ON part.id = rel.partenaire_id
                                        LEFT JOIN localisation loc_part ON loc_part.id=part.localisation_id
                                        LEFT JOIN accessibilite accs_stockage ON accs_stockage.id = stock.accessibilite_id
                                        LEFT JOIN conservation cons_don ON cons_don.id = d.conservation_id
                                        
                                        LEFT JOIN indisponibilite indispo_adherent ON indispo_adherent.adherent_id = donor.id
                                        LEFT JOIN indisponibilite indispo_stockage ON indispo_stockage.stockage_id = stock.id

                                    WHERE   (d.id=                        @id                           OR       @id                         IS NULL)";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération des detailles d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }

        public List<Horaire> GetStockageHoraires(int id)
        {
            List<Horaire> result = new List<Horaire>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT horaire_stockage.morning_start, horaire_stockage.morning_end, horaire_stockage.afternoon_start, horaire_stockage.afternoon_end,
                                jour_horaire_stockage.name jour_horaire_stockage
                                FROM horaire horaire_stockage
                                LEFT JOIN jour jour_horaire_stockage ON jour_horaire_stockage.id = horaire_stockage.jour_id
                                WHERE horaire_stockage.stockage_id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Horaire horaire = HoraireDataToEntity(dr);

                    result.Add(horaire);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("GetStockageHoraires search error : " + ex.Message);
            }
            finally
            {

                cnx.Close();
            }

            return result;
        }

        public List<Horaire> GetDonHoraires(int id)
        {
            List<Horaire> result = new List<Horaire>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT  horaire_don.morning_start, horaire_don.morning_end, horaire_don.afternoon_start, horaire_don.afternoon_end,
                                jour_horaire_don.name jour_horaire_don 
                                FROM horaire horaire_don
                                LEFT JOIN jour jour_horaire_don ON jour_horaire_don.id = horaire_don.jour_id
                                WHERE horaire_don.don_id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Horaire horaire = HoraireDataToEntity(dr);

                    result.Add(horaire);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("GetDonHoraires search error : " + ex.Message);
            }
            finally
            {

                cnx.Close();
            }

            return result;

        }

        public List<DonDetails> GetByAdherent(int adherentId)
        {
            List<DonDetails> result = new List<DonDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT  d.*,
                                        loc.city don_city, loc.postcode don_postcode,
                                        uni.name unite_name, 
                                        cons_don.name cons_don_name,

                                        donor.id donor_id,donor.firstname donor_firstname,donor.lastname donor_lastname,
                                        donor.email donor_email, donor.tel donor_tel, donor.street_nb donor_street_nb, donor.street_name donor_street_name, donor.comp_adr donor_comp_adr,
                                        donor_loc.city donor_city, donor_loc.postcode donor_postcode,

                                        beneficiary.id bene_id, beneficiary.firstname bene_firstname, beneficiary.lastname bene_lastname,
                                        beneficiary.email bene_email, beneficiary.tel bene_tel, beneficiary.street_nb bene_street_nb, beneficiary.street_name bene_street_name, beneficiary.comp_adr bene_comp_adr,
                                        bene_loc.city bene_city, bene_loc.postcode bene_postcode,

                                        p.name p_name,
                                        c.id c_id, c2.id c2_id, c3.id c3_id,
                                        c.name cat_name,c2.name cat2_name,c3.name cat3_name,
                                
                                        rel.street_nb rel_street_nb, rel.street_name rel_street_name, rel.comp_adr rel_comp_adr,
                                        loc_relais.city relais_city, loc_relais.postcode relais_postcode,

                                        part.street_nb part_street_nb, part.street_name part_street_name, part.comp_adr part_comp_adr,
                                        loc_part.city part_city, loc_part.postcode part_postcode,

                                        accs_stockage.name accs_stockage_name, cons_stockage.name cons_stockage_name,

                                        indispo_adherent.start indispo_adherent_start, indispo_adherent.end indispo_adherent_end,
                                        indispo_stockage.start indispo_stockage_start, indispo_stockage.end indispo_stockage_end

                                    FROM don d
                                        LEFT JOIN adherent donor ON d.donor_id=donor.id
                                        LEFT JOIN localisation donor_loc ON donor_loc.id=donor.localisation_id
                                        LEFT JOIN adherent beneficiary ON d.beneficiary_id=beneficiary.id
                                        LEFT JOIN localisation bene_loc ON bene_loc.id=beneficiary.localisation_id
                                        LEFT JOIN produit p ON d.produit_id=p.id
								        LEFT JOIN unite uni ON d.unite_id=uni.id
                                        LEFT JOIN stockage stock ON d.stockage_id=stock.id
                                        LEFT JOIN conservation cons_stockage ON stock.conservation_id=cons_stockage.id
                                        LEFT JOIN localisation loc ON d.localisation_id=loc.id
                                        LEFT JOIN cat3 c3 ON p.cat3_id=c3.id
                                        LEFT JOIN cat2 c2 ON c3.cat2_id=c2.id
                                        LEFT JOIN cat c ON c2.cat_id=c.id
                                        LEFT JOIN relais rel ON stock.relais_id=rel.id
                                        LEFT JOIN localisation loc_relais ON loc_relais.id=rel.localisation_id
                                        LEFT JOIN partenaire part ON part.id = rel.partenaire_id
                                        LEFT JOIN localisation loc_part ON loc_part.id=part.localisation_id
                                        LEFT JOIN accessibilite accs_stockage ON accs_stockage.id = stock.accessibilite_id
                                        LEFT JOIN conservation cons_don ON cons_don.id = d.conservation_id
                                        
                                        LEFT JOIN indisponibilite indispo_adherent ON indispo_adherent.adherent_id = donor.id
                                        LEFT JOIN indisponibilite indispo_stockage ON indispo_stockage.stockage_id = stock.id

                                    WHERE   (d.beneficiary_id=            @beneficiary_id               OR       @beneficiary_id             IS NULL)
                                        OR  (d.donor_id=                  @donor_id                     OR       @donor_id                   IS NULL)";

            cmd.Parameters.Add(new MySqlParameter("beneficiary_id", adherentId));
            cmd.Parameters.Add(new MySqlParameter("donor_id", adherentId));


            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DonDetails donDetails = DataReaderToEntity(dr);

                    result.Add(donDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("MultiCriteria search error : " + ex.Message);
            }
            finally
            {

                cnx.Close();
            }

            return result;
        }

        //GET BY MULTI 
        public List<DonDetails> GetByMultiCriteria(DonMultiSearch donMultiSearch)
        {

            List<DonDetails> result = new List<DonDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT  d.*,
                                        loc.city don_city, loc.postcode don_postcode,
                                        uni.name unite_name, 
                                        cons_don.name cons_don_name,

                                        donor.id donor_id,donor.firstname donor_firstname,donor.lastname donor_lastname,
                                        donor.email donor_email, donor.tel donor_tel, donor.street_nb donor_street_nb, donor.street_name donor_street_name, donor.comp_adr donor_comp_adr,
                                        donor_loc.city donor_city, donor_loc.postcode donor_postcode,

                                        beneficiary.id bene_id, beneficiary.firstname bene_firstname, beneficiary.lastname bene_lastname,
                                        beneficiary.email bene_email, beneficiary.tel bene_tel, beneficiary.street_nb bene_street_nb, beneficiary.street_name bene_street_name, beneficiary.comp_adr bene_comp_adr,
                                        bene_loc.city bene_city, bene_loc.postcode bene_postcode,

                                        p.name p_name,
                                        c.id c_id, c2.id c2_id, c3.id c3_id,
                                        c.name cat_name,c2.name cat2_name,c3.name cat3_name,
                                
                                        rel.street_nb rel_street_nb, rel.street_name rel_street_name, rel.comp_adr rel_comp_adr,
                                        loc_relais.city relais_city, loc_relais.postcode relais_postcode,

                                        part.street_nb part_street_nb, part.street_name part_street_name, part.comp_adr part_comp_adr,
                                        loc_part.city part_city, loc_part.postcode part_postcode,

                                        accs_stockage.name accs_stockage_name, cons_stockage.name cons_stockage_name,

                                        indispo_adherent.start indispo_adherent_start, indispo_adherent.end indispo_adherent_end,
                                        indispo_stockage.start indispo_stockage_start, indispo_stockage.end indispo_stockage_end

                                    FROM don d
                                        LEFT JOIN adherent donor ON d.donor_id=donor.id
                                        LEFT JOIN localisation donor_loc ON donor_loc.id=donor.localisation_id
                                        LEFT JOIN adherent beneficiary ON d.beneficiary_id=beneficiary.id
                                        LEFT JOIN localisation bene_loc ON bene_loc.id=beneficiary.localisation_id
                                        LEFT JOIN produit p ON d.produit_id=p.id
								        LEFT JOIN unite uni ON d.unite_id=uni.id
                                        LEFT JOIN stockage stock ON d.stockage_id=stock.id
                                        LEFT JOIN conservation cons_stockage ON stock.conservation_id=cons_stockage.id
                                        LEFT JOIN localisation loc ON d.localisation_id=loc.id
                                        LEFT JOIN cat3 c3 ON p.cat3_id=c3.id
                                        LEFT JOIN cat2 c2 ON c3.cat2_id=c2.id
                                        LEFT JOIN cat c ON c2.cat_id=c.id
                                        LEFT JOIN relais rel ON stock.relais_id=rel.id
                                        LEFT JOIN localisation loc_relais ON loc_relais.id=rel.localisation_id
                                        LEFT JOIN partenaire part ON part.id = rel.partenaire_id
                                        LEFT JOIN localisation loc_part ON loc_part.id=part.localisation_id
                                        LEFT JOIN accessibilite accs_stockage ON accs_stockage.id = stock.accessibilite_id
                                        LEFT JOIN conservation cons_don ON cons_don.id = d.conservation_id
                                        LEFT JOIN indisponibilite indispo_adherent ON indispo_adherent.adherent_id = donor.id
                                        LEFT JOIN indisponibilite indispo_stockage ON indispo_stockage.stockage_id = stock.id

                                    WHERE   (d.id=                        @id                           OR       @id                         IS NULL)
                                        AND (d.beneficiary_id=            @beneficiary_id               OR       @beneficiary_id             IS NULL)
                                        AND (d.produit_id=                @produit_id                   OR       @produit_id                 IS NULL)
                                        AND (d.donor_id=                  @donor_id                     OR       @donor_id                   IS NULL)
                                        AND (d.stockage_id=               @stockage_id                  OR       @stockage_id                IS NULL)
                                        AND (d.unite_id=                  @unite_id                     OR       @unite_id                   IS NULL)
                                        AND (d.conservation_id=           @conservation_id              OR       @conservation_id            IS NULL)
                                        AND (d.localisation_id=           @localisation_id              OR       @localisation_id            IS NULL)
                                        AND (d.qty=                       @qty                          OR       @qty                        IS NULL)
                                        AND (d.dlc>=                      @dlc_min                      OR       @dlc_min                    IS NULL)
                                        AND (d.dlc<=                      @dlc_max                      OR       @dlc_max                    IS NULL)
                                        AND (d.ddm>=                      @ddm_min                      OR       @ddm_min                    IS NULL)
                                        AND (d.ddm<=                      @ddm_max                      OR       @ddm_max                    IS NULL)
                                        AND (d.create_date>=              @create_date_min              OR       @create_date_min            IS NULL)
                                        AND (d.create_date<=              @create_date_max              OR       @create_date_max            IS NULL)
                                        AND (d.reserve_date<=             @reserve_date_min             OR       @reserve_date_min           IS NULL)
                                        AND (d.reserve_date>=             @reserve_date_max             OR       @reserve_date_max           IS NULL)
                                        AND (d.cancel_date<=              @cancel_date_min              OR       @cancel_date_min            IS NULL)
                                        AND (d.cancel_date>=              @cancel_date_max              OR       @cancel_date_max            IS NULL)
                                        AND (d.picture=                   @picture                      OR       @picture                    IS NULL)
                                        AND (d.alt_street_nb=             @alt_street_number            OR       @alt_street_number          IS NULL)
                                        AND (d.alt_street_name=           @alt_street_name              OR       @alt_street_name            IS NULL)
                                        AND (d.alt_comp_adr=              @alt_comp_adr                 OR       @alt_comp_adr               IS NULL)
                                        AND (d.available_collect>=        @available_collect_min        OR       @available_collect_min      IS NULL)
                                        AND (d.available_collect<=        @available_collect_max        OR       @available_collect_max      IS NULL)
                                        AND (d.receive>=                  @receive_min                  OR       @receive_min                IS NULL)
                                        AND (d.receive<=                  @receive_max                  OR       @receive_max                IS NULL)
                                        AND (d.no_receive>=               @no_receive_min               OR       @no_receive_min             IS NULL)
                                        AND (d.no_receive<=               @no_receive_max               OR       @no_receive_max             IS NULL)
                                        AND (d.handover>=                 @handover_min                 OR       @handover_min               IS NULL)
                                        AND (d.handover<=                 @handover_max                 OR       @handover_max               IS NULL)
                                        AND (d.no_handover>=              @no_handover_min              OR       @no_handover_min            IS NULL)
                                        AND (d.no_handover<=              @no_handover_max              OR       @no_handover_max            IS NULL)
                                        AND (d.final_date>=               @final_date_min               OR       @final_date_min             IS NULL)
                                        AND (d.final_date<=               @final_date_max               OR       @final_date_max             IS NULL)
                                        AND (p.name LIKE                  @produit_name                 OR       @produit_name               IS NULL)
                                        AND (c.id=                        @c_id                         OR       @c_id                       IS NULL)
                                        AND (c2.id=                       @c2_id                        OR       @c2_id                      IS NULL)
                                        AND (c3.id=                       @c3_id                        OR       @c3_id                      IS NULL)";

            cmd.Parameters.Add(new MySqlParameter("id", donMultiSearch.Id));
            cmd.Parameters.Add(new MySqlParameter("beneficiary_id", donMultiSearch.BeneficiaryId));
            cmd.Parameters.Add(new MySqlParameter("produit_id", donMultiSearch.ProduitId));
            cmd.Parameters.Add(new MySqlParameter("donor_id", donMultiSearch.DonorId));
            cmd.Parameters.Add(new MySqlParameter("stockage_id", donMultiSearch.StockageId));
            cmd.Parameters.Add(new MySqlParameter("unite_id", donMultiSearch.UniteId));
            cmd.Parameters.Add(new MySqlParameter("conservation_id", donMultiSearch.ConservationId));
            cmd.Parameters.Add(new MySqlParameter("localisation_id", donMultiSearch.LocalisationId));
            cmd.Parameters.Add(new MySqlParameter("qty", donMultiSearch.Qty));
            cmd.Parameters.Add(new MySqlParameter("dlc_min", donMultiSearch.DlcMin));
            cmd.Parameters.Add(new MySqlParameter("dlc_max", donMultiSearch.DlcMax));
            cmd.Parameters.Add(new MySqlParameter("ddm_min", donMultiSearch.DdmMin));
            cmd.Parameters.Add(new MySqlParameter("ddm_max", donMultiSearch.DdmMax));
            cmd.Parameters.Add(new MySqlParameter("create_date_min", donMultiSearch.CreateDateMin));
            cmd.Parameters.Add(new MySqlParameter("create_date_max", donMultiSearch.CreateDateMax));
            cmd.Parameters.Add(new MySqlParameter("reserve_date_min", donMultiSearch.ReserveDateMin));
            cmd.Parameters.Add(new MySqlParameter("reserve_date_max", donMultiSearch.ReserveDateMax));
            cmd.Parameters.Add(new MySqlParameter("cancel_date_min", donMultiSearch.CancelDateMin));
            cmd.Parameters.Add(new MySqlParameter("cancel_date_max", donMultiSearch.CancelDateMax));
            cmd.Parameters.Add(new MySqlParameter("picture", donMultiSearch.Picture));
            cmd.Parameters.Add(new MySqlParameter("alt_street_number", donMultiSearch.AltStreetNb));
            cmd.Parameters.Add(new MySqlParameter("alt_street_name", donMultiSearch.AltStreetName));
            cmd.Parameters.Add(new MySqlParameter("alt_comp_adr", donMultiSearch.AltCompAdr));
            cmd.Parameters.Add(new MySqlParameter("available_collect_min", donMultiSearch.AvailableCollectMin));
            cmd.Parameters.Add(new MySqlParameter("available_collect_max", donMultiSearch.AvailableCollectMax));
            cmd.Parameters.Add(new MySqlParameter("receive_min", donMultiSearch.ReceiveMin));
            cmd.Parameters.Add(new MySqlParameter("receive_max", donMultiSearch.ReceiveMax));
            cmd.Parameters.Add(new MySqlParameter("no_receive_min", donMultiSearch.NoReceiveMin));
            cmd.Parameters.Add(new MySqlParameter("no_receive_max", donMultiSearch.NoReceiveMax));
            cmd.Parameters.Add(new MySqlParameter("handover_min", donMultiSearch.HandoverMin));
            cmd.Parameters.Add(new MySqlParameter("handover_max", donMultiSearch.HandoverMax));
            cmd.Parameters.Add(new MySqlParameter("no_handover_min", donMultiSearch.NoHandoverMin));
            cmd.Parameters.Add(new MySqlParameter("no_handover_max", donMultiSearch.NoHandoverMax));
            cmd.Parameters.Add(new MySqlParameter("final_date_min", donMultiSearch.FinalDateMin));
            cmd.Parameters.Add(new MySqlParameter("final_date_max", donMultiSearch.FinalDateMax));
            string produit_name = "%" + donMultiSearch.ProduitName + "%";
            cmd.Parameters.Add(new MySqlParameter("produit_name", produit_name));
            cmd.Parameters.Add(new MySqlParameter("c_id", donMultiSearch.CatId));
            cmd.Parameters.Add(new MySqlParameter("c2_id", donMultiSearch.Cat2Id));
            cmd.Parameters.Add(new MySqlParameter("c3_id", donMultiSearch.Cat3Id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DonDetails donDetails = DataReaderToEntity(dr);

                    result.Add(donDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("MultiCriteria search error : " + ex.Message);
            }
            finally
            {

                cnx.Close();
            }

            return result;
        }

        #endregion

        #region UPDATE
        public void ConfirmationDepositInAStorageSpace(Don don)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE don 
                                SET available_collect = @available_collect 
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", don.Id));
            cmd.Parameters.Add(new MySqlParameter("available_collect", DateTime.Now));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de validation de dépot d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public void ConfirmationEchangeDonor(Don don)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE don 
                                SET handover = @handover 
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", don.Id));
            cmd.Parameters.Add(new MySqlParameter("handover", DateTime.Now));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de validation d'échange d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public void ConfirmationEchangeBeneficiary(Don don)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE don 
                                SET receive = @receive 
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", don.Id));
            cmd.Parameters.Add(new MySqlParameter("receive", DateTime.Now));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de validation d'échange d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public void CancelReservation(Don don)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE don 
                                SET reserve_date = NULL, 
                                    available_collect = NULL,
                                    receive = NULL,
                                    handover = NULL,
                                    beneficiary_id = NULL
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", don.Id));
            //cmd.Parameters.Add(new MySqlParameter("reserve_date", don.ReserveDate));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'annulation de la réservation du don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public void MakeAReservation(Don don)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE don 
                                SET beneficiary_id = @beneficiary_id, 
                                    reserve_date = @reserve_date 
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", don.Id));
            cmd.Parameters.Add(new MySqlParameter("beneficiary_id", don.BeneficiaryId));
            cmd.Parameters.Add(new MySqlParameter("reserve_date", DateTime.Now));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de réservation d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        public void MakeAReservationWithBeneficiaryId(int? id, int? selectedAdherentId)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE don 
                                SET beneficiary_id = @beneficiary_id, 
                                    reserve_date = @reserve_date 
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));
            cmd.Parameters.Add(new MySqlParameter("beneficiary_id", selectedAdherentId));
            cmd.Parameters.Add(new MySqlParameter("reserve_date", DateTime.Now));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de réservation d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public void Update(Don don)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE don 
                                SET beneficiary_id = @beneficiary_id, 
                                    produit_id = @produit_id, 
                                    donor_id = @donor_id, 
                                    stockage_id = @stockage_id, 
                                    unite_id = @unite_id, 
                                    conservation_id = @conservation_id, 
                                    localisation_id = @localisation_id, 
                                    qty = @qty, 
                                    dlc = @dlc, 
                                    ddm = @ddm, 
                                    create_date = @create_date, 
                                    reserve_date = @reserve_date, 
                                    cancel_date = @cancel_date, 
                                    picture = @picture, 
                                    alt_street_nb = @alt_street_nb, 
                                    alt_street_name = @alt_street_name, 
                                    alt_comp_adr = @alt_comp_adr, 
                                    available_collect = @available_collect, 
                                    receive = @receive, 
                                    no_receive = @no_receive, 
                                    no_handover = @no_handover, 
                                    handover = @handover, 
                                    final_date = @final_date 
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", don.Id));
            cmd.Parameters.Add(new MySqlParameter("beneficiary_id", don.BeneficiaryId));
            cmd.Parameters.Add(new MySqlParameter("produit_id", don.ProduitId));
            cmd.Parameters.Add(new MySqlParameter("donor_id", don.DonorId));
            cmd.Parameters.Add(new MySqlParameter("stockage_id", don.StockageId));
            cmd.Parameters.Add(new MySqlParameter("unite_id", don.UniteId));
            cmd.Parameters.Add(new MySqlParameter("conservation_id", don.ConservationId));
            cmd.Parameters.Add(new MySqlParameter("localisation_id", don.LocalisationId));
            cmd.Parameters.Add(new MySqlParameter("qty", don.Qty));
            cmd.Parameters.Add(new MySqlParameter("dlc", don.Dlc));
            cmd.Parameters.Add(new MySqlParameter("ddm", don.Ddm));
            cmd.Parameters.Add(new MySqlParameter("create_date", don.CreateDate));
            cmd.Parameters.Add(new MySqlParameter("reserve_date", don.ReserveDate));
            cmd.Parameters.Add(new MySqlParameter("cancel_date", don.CancelDate));
            cmd.Parameters.Add(new MySqlParameter("picture", don.Picture));
            cmd.Parameters.Add(new MySqlParameter("alt_street_nb", don.AltStreetNb));
            cmd.Parameters.Add(new MySqlParameter("alt_street_name", don.AltStreetName));
            cmd.Parameters.Add(new MySqlParameter("alt_comp_adr", don.AltCompAdr));
            cmd.Parameters.Add(new MySqlParameter("available_collect", don.AvailableCollect));
            cmd.Parameters.Add(new MySqlParameter("receive", don.Receive));
            cmd.Parameters.Add(new MySqlParameter("no_receive", don.NoReceive));
            cmd.Parameters.Add(new MySqlParameter("no_handover", don.NoHandover));
            cmd.Parameters.Add(new MySqlParameter("handover", don.Handover));
            cmd.Parameters.Add(new MySqlParameter("final_date", don.FinalDate));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de mise à jour d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public void CancelDon(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE don 
                                SET cancel_date = SYSDATE()
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));
            // cmd.Parameters.Add(new MySqlParameter("reserve_date", don.ReserveDate));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la suppresion du don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
        #endregion

        #region FACTORISATION
        private Horaire HoraireDataToEntity(DbDataReader dr)
        {

            Horaire horaire = new Horaire();

            if (!dr.IsDBNull(dr.GetOrdinal("morning_start")))
            {
                horaire.MorningStart = (TimeSpan)dr.GetValue(dr.GetOrdinal("morning_start"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("morning_end")))
            {
                horaire.MorningEnd = (TimeSpan)dr.GetValue(dr.GetOrdinal("morning_end"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("afternoon_start")))
            {
                horaire.AfternoonStart = (TimeSpan)dr.GetValue(dr.GetOrdinal("afternoon_start"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("afternoon_end")))
            {
                horaire.AfternoonEnd = (TimeSpan)dr.GetValue(dr.GetOrdinal("afternoon_end"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("afternoon_end")))
            {
                horaire.AfternoonEnd = (TimeSpan)dr.GetValue(dr.GetOrdinal("afternoon_end"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("jour_horaire_don")))
            {
                horaire.Day = dr.GetString(dr.GetOrdinal("jour_horaire_don"));
            }
            return horaire;

        }

        private Don DataReaderToDon(DbDataReader dr)
        {
            DonDetails don = new DonDetails();

            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                don.Id = dr.GetInt32(dr.GetOrdinal("id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("qty")))
            {
                don.Qty = dr.GetInt32(dr.GetOrdinal("qty"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("dlc")))
            {
                don.Dlc = dr.GetDateTime(dr.GetOrdinal("dlc"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("ddm")))
            {
                don.Ddm = dr.GetDateTime(dr.GetOrdinal("ddm"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("create_date")))
            {
                don.CreateDate = dr.GetDateTime(dr.GetOrdinal("create_date"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("reserve_date")))
            {
                don.ReserveDate = dr.GetDateTime(dr.GetOrdinal("reserve_date"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("cancel_date")))
            {
                don.CancelDate = dr.GetDateTime(dr.GetOrdinal("cancel_date"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("picture")))
            {
                don.Picture = dr.GetString(dr.GetOrdinal("picture"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("alt_street_nb")))
            {
                don.AltStreetNb = dr.GetInt32(dr.GetOrdinal("alt_street_nb"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("alt_street_name")))
            {
                don.AltStreetName = dr.GetString(dr.GetOrdinal("alt_street_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("alt_comp_adr")))
            {
                don.AltCompAdr = dr.GetString(dr.GetOrdinal("alt_comp_adr"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("available_collect")))
            {
                don.AvailableCollect = dr.GetDateTime(dr.GetOrdinal("available_collect"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("receive")))
            {
                don.Receive = dr.GetDateTime(dr.GetOrdinal("receive"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("no_receive")))
            {
                don.NoReceive = dr.GetDateTime(dr.GetOrdinal("no_receive"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("handover")))
            {
                don.Handover = dr.GetDateTime(dr.GetOrdinal("handover"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("no_handover")))
            {
                don.NoHandover = dr.GetDateTime(dr.GetOrdinal("no_handover"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("final_date")))
            {
                don.FinalDate = dr.GetDateTime(dr.GetOrdinal("final_date"));

            }
            //beneficiary_id !! TYPO
            if (!dr.IsDBNull(dr.GetOrdinal("beneficiary_id")))
            {
                don.BeneficiaryId = dr.GetInt32(dr.GetOrdinal("beneficiary_id"));

            }
            if (!dr.IsDBNull(dr.GetOrdinal("stockage_id")))
            {
                don.StockageId = dr.GetInt32(dr.GetOrdinal("stockage_id"));

            }
            if (!dr.IsDBNull(dr.GetOrdinal("produit_id")))
            {
                don.ProduitId = dr.GetInt32(dr.GetOrdinal("produit_id"));

            }
            if (!dr.IsDBNull(dr.GetOrdinal("donor_id")))
            {
                don.DonorId = dr.GetInt32(dr.GetOrdinal("donor_id"));

            }
            if (!dr.IsDBNull(dr.GetOrdinal("unite_id")))
            {
                don.UniteId = dr.GetInt32(dr.GetOrdinal("unite_id"));

            }
            if (!dr.IsDBNull(dr.GetOrdinal("conservation_id")))
            {
                don.ConservationId = dr.GetInt32(dr.GetOrdinal("conservation_id"));

            }
            if (!dr.IsDBNull(dr.GetOrdinal("localisation_id")))
            {
                don.LocalisationId = dr.GetInt32(dr.GetOrdinal("localisation_id"));

            }
            return don;

        }

        private DonDetails DataReaderToEntity(DbDataReader dr)
        {
            DonDetails don = new DonDetails();

            if (!dr.IsDBNull(dr.GetOrdinal("cat3_name")))
            {
                don.Cat3Name = dr.GetString(dr.GetOrdinal("cat3_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("cat2_name")))
            {
                don.Cat2Name = dr.GetString(dr.GetOrdinal("cat2_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("cat_name")))
            {
                don.CatName = dr.GetString(dr.GetOrdinal("cat_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("donor_email")))
            {
                don.DonorEmail = dr.GetString(dr.GetOrdinal("donor_email"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("donor_tel")))
            {
                don.DonorTel = dr.GetString(dr.GetOrdinal("donor_tel"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("bene_email")))
            {
                don.BeneficiaryEmail = dr.GetString(dr.GetOrdinal("bene_email"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("bene_tel")))
            {
                don.BeneficiaryTel = dr.GetString(dr.GetOrdinal("bene_tel"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("donor_lastname")))
            {
                don.DonorLastName = dr.GetString(dr.GetOrdinal("donor_lastname"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("donor_firstname")))
            {
                don.DonorFirstName = dr.GetString(dr.GetOrdinal("donor_firstname"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("bene_lastname")))
            {
                don.BeneficiaryLastName = dr.GetString(dr.GetOrdinal("bene_lastname"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("bene_firstname")))
            {
                don.BeneficiaryFirstName = dr.GetString(dr.GetOrdinal("bene_firstname"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("donor_postcode")))
            {
                don.DonorPostcode = dr.GetString(dr.GetOrdinal("donor_postcode"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("donor_city")))
            {
                don.DonorCity = dr.GetString(dr.GetOrdinal("donor_city"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("donor_comp_adr")))
            {
                don.DonorCompAdr = dr.GetString(dr.GetOrdinal("donor_comp_adr"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("donor_street_name")))
            {
                don.DonorStreetName = dr.GetString(dr.GetOrdinal("donor_street_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("donor_street_nb")))
            {
                don.DonorStreetNb = dr.GetInt32(dr.GetOrdinal("donor_street_nb"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("part_postcode")))
            {
                don.PartPostcode = dr.GetString(dr.GetOrdinal("part_postcode"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("part_city")))
            {
                don.PartCity = dr.GetString(dr.GetOrdinal("part_city"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("part_comp_adr")))
            {
                don.PartCompAdr = dr.GetString(dr.GetOrdinal("part_comp_adr"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("part_street_name")))
            {
                don.PartStreetName = dr.GetString(dr.GetOrdinal("part_street_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("part_street_nb")))
            {
                don.PartStreetNb = dr.GetInt32(dr.GetOrdinal("part_street_nb"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("relais_postcode")))
            {
                don.RelaisPostcode = dr.GetString(dr.GetOrdinal("relais_postcode"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("relais_city")))
            {
                don.RelaisCity = dr.GetString(dr.GetOrdinal("relais_city"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("rel_comp_adr")))
            {
                don.RelCompAdr = dr.GetString(dr.GetOrdinal("rel_comp_adr"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("rel_street_name")))
            {
                don.RelStreetName = dr.GetString(dr.GetOrdinal("rel_street_name"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("rel_street_nb")))
            {
                don.RelStreetNb = dr.GetInt32(dr.GetOrdinal("rel_street_nb"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("don_postcode")))
            {
                don.DonPostcode = dr.GetString(dr.GetOrdinal("don_postcode"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("don_city")))
            {
                don.DonCity = dr.GetString(dr.GetOrdinal("don_city"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("p_name")))
            {
                don.ProduitName = dr.GetString(dr.GetOrdinal("p_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("unite_name")))
            {
                don.UniteName = dr.GetString(dr.GetOrdinal("unite_name"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                don.Id = dr.GetInt32(dr.GetOrdinal("id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("qty")))
            {
                don.Qty = dr.GetInt32(dr.GetOrdinal("qty"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("dlc")))
            {
                don.Dlc = dr.GetDateTime(dr.GetOrdinal("dlc"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("ddm")))
            {
                don.Ddm = dr.GetDateTime(dr.GetOrdinal("ddm"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("create_date")))
            {
                don.CreateDate = dr.GetDateTime(dr.GetOrdinal("create_date"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("reserve_date")))
            {
                don.ReserveDate = dr.GetDateTime(dr.GetOrdinal("reserve_date"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("cancel_date")))
            {
                don.CancelDate = dr.GetDateTime(dr.GetOrdinal("cancel_date"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("picture")))
            {
                don.Picture = dr.GetString(dr.GetOrdinal("picture"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("alt_street_nb")))
            {
                don.AltStreetNb = dr.GetInt32(dr.GetOrdinal("alt_street_nb"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("alt_street_name")))
            {
                don.AltStreetName = dr.GetString(dr.GetOrdinal("alt_street_name"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("alt_comp_adr")))
            {
                don.AltCompAdr = dr.GetString(dr.GetOrdinal("alt_comp_adr"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("available_collect")))
            {
                don.AvailableCollect = dr.GetDateTime(dr.GetOrdinal("available_collect"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("receive")))
            {
                don.Receive = dr.GetDateTime(dr.GetOrdinal("receive"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("no_receive")))
            {
                don.NoReceive = dr.GetDateTime(dr.GetOrdinal("no_receive"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("handover")))
            {
                don.Handover = dr.GetDateTime(dr.GetOrdinal("handover"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("no_handover")))
            {
                don.NoHandover = dr.GetDateTime(dr.GetOrdinal("no_handover"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("final_date")))
            {
                don.FinalDate = dr.GetDateTime(dr.GetOrdinal("final_date"));

            }
            //beneficiary_id !! TYPO
            if (!dr.IsDBNull(dr.GetOrdinal("beneficiary_id")))
            {
                don.BeneficiaryId = dr.GetInt32(dr.GetOrdinal("beneficiary_id"));

            }
            if (!dr.IsDBNull(dr.GetOrdinal("stockage_id")))
            {
                don.StockageId = dr.GetInt32(dr.GetOrdinal("stockage_id"));

            }
            if (!dr.IsDBNull(dr.GetOrdinal("produit_id")))
            {
                don.ProduitId = dr.GetInt32(dr.GetOrdinal("produit_id"));

            }
            if (!dr.IsDBNull(dr.GetOrdinal("donor_id")))
            {
                don.DonorId = dr.GetInt32(dr.GetOrdinal("donor_id"));

            }
            if (!dr.IsDBNull(dr.GetOrdinal("unite_id")))
            {
                don.UniteId = dr.GetInt32(dr.GetOrdinal("unite_id"));

            }
            if (!dr.IsDBNull(dr.GetOrdinal("conservation_id")))
            {
                don.ConservationId = dr.GetInt32(dr.GetOrdinal("conservation_id"));

            }
            if (!dr.IsDBNull(dr.GetOrdinal("localisation_id")))
            {
                don.LocalisationId = dr.GetInt32(dr.GetOrdinal("localisation_id"));

            }

            if (don.Id != null)
            {
                don.HorairesDon = this.GetDonHoraires((int)don.Id);
            }
            if (don.StockageId != null)
            {
                don.HorairesRelais = this.GetStockageHoraires((int)don.StockageId);
            }

            if (!dr.IsDBNull(dr.GetOrdinal("c_id")))
            {
                don.CatId = dr.GetInt32(dr.GetOrdinal("c_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c2_id")))
            {
                don.Cat2Id = dr.GetInt32(dr.GetOrdinal("c2_id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("c3_id")))
            {
                don.Cat3Id = dr.GetInt32(dr.GetOrdinal("c3_id"));
            }
            return don;
        }
        #endregion
    }
}
