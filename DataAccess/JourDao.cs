﻿using Fr.EQL.AI110.DLD_GC.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.DataAccess
{
    public class JourDao : DAO
    {
        public List<Jour> GetAllDays()
        {
            List<Jour> result = new List<Jour>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT * FROM jour";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result.Add(DataReaderToDays(dr));
                }
            } 
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la sélection des jours " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
        private Jour DataReaderToDays(DbDataReader dr)
        {
            Jour result = new Jour();
            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                result.Id = dr.GetInt32(dr.GetOrdinal("id"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("name")))
            {
                result.Name = dr.GetString(dr.GetOrdinal("name"));
            }
            return result;
        }
    }
}
