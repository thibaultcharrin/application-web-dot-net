using Fr.EQL.AI110.DLD_GC.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GC.DataAccess
{
    public class AdherentDao : DAO
    {
        public Adherent GetById(int id)
        {
            Adherent result = new Adherent();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT * 
									FROM adherent
										WHERE id=@id";


            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Error during adherent extraction by id : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }

        public List<AdherentDetails> GetAllAdherentDetails()
        {
            List<AdherentDetails> result = new List<AdherentDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT a.*, loc.city city, loc.postcode postcode
                                FROM adherent a
                                LEFT JOIN localisation loc ON loc.id=a.localisation_id";

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    result.Add(DataReaderToAdherentDetails(dr));
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Error during adherent extraction by id : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return (result);

        }

        private AdherentDetails DataReaderToAdherentDetails(DbDataReader dr)
        {
            AdherentDetails adherent = new AdherentDetails();

            if (!dr.IsDBNull(dr.GetOrdinal("city")))
            {
                adherent.City = dr.GetString(dr.GetOrdinal("city"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("postcode")))
            {
                adherent.PostCode = dr.GetString(dr.GetOrdinal("postcode"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                adherent.Id = dr.GetInt32(dr.GetOrdinal("id"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("localisation_id")))
            {
                adherent.LocalisationId = dr.GetInt32(dr.GetOrdinal("localisation_id"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("firstname")))
            {
                adherent.FirstName = dr.GetString(dr.GetOrdinal("firstname"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("lastname")))
            {
                adherent.LastName = dr.GetString(dr.GetOrdinal("lastname"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("dob")))
            {
                adherent.Dob = dr.GetDateTime(dr.GetOrdinal("dob"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("email")))
            {
                adherent.Email = dr.GetString(dr.GetOrdinal("email"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("pw")))
            {
                adherent.Pw = dr.GetString(dr.GetOrdinal("pw"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("street_nb")))
            {
                adherent.StreetNb = dr.GetInt32(dr.GetOrdinal("street_nb"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("street_name")))
            {
                adherent.StreetName = dr.GetString(dr.GetOrdinal("street_name"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("comp_adr")))
            {
                adherent.StreetName = dr.GetString(dr.GetOrdinal("comp_adr"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("tel")))
            {
                adherent.Tel = dr.GetString(dr.GetOrdinal("tel"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("create_date")))
            {
                adherent.CreateDate = dr.GetDateTime(dr.GetOrdinal("create_date"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("confirm_date")))
            {
                adherent.ConfirmDate = dr.GetDateTime(dr.GetOrdinal("confirm_date"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("deactivate_date")))
            {
                adherent.DeactivateDate = dr.GetDateTime(dr.GetOrdinal("deactivate_date"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("activation_code")))
            {
                adherent.ActivationCode = dr.GetInt32(dr.GetOrdinal("activation_code"));
            }

            return adherent;
        }

        private Adherent DataReaderToEntity(DbDataReader dr)
        {
            Adherent adherent = new Adherent();

            if (!dr.IsDBNull(dr.GetOrdinal("id")))
            {
                adherent.Id = dr.GetInt32(dr.GetOrdinal("id"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("localisation_id")))
            {
                adherent.LocalisationId = dr.GetInt32(dr.GetOrdinal("localisation_id"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("firstname")))
            {
                adherent.FirstName = dr.GetString(dr.GetOrdinal("firstname"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("lastname")))
            {
                adherent.LastName = dr.GetString(dr.GetOrdinal("lastname"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("dob")))
            {
                adherent.Dob = dr.GetDateTime(dr.GetOrdinal("dob"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("email")))
            {
                adherent.Email = dr.GetString(dr.GetOrdinal("email"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("pw")))
            {
                adherent.Pw = dr.GetString(dr.GetOrdinal("pw"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("street_nb")))
            {
                adherent.StreetNb = dr.GetInt32(dr.GetOrdinal("street_nb"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("street_name")))
            {
                adherent.StreetName = dr.GetString(dr.GetOrdinal("street_name"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("comp_adr")))
            {
                adherent.StreetName = dr.GetString(dr.GetOrdinal("comp_adr"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("tel")))
            {
                adherent.Tel = dr.GetString(dr.GetOrdinal("tel"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("create_date")))
            {
                adherent.CreateDate = dr.GetDateTime(dr.GetOrdinal("create_date"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("confirm_date")))
            {
                adherent.ConfirmDate = dr.GetDateTime(dr.GetOrdinal("confirm_date"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("deactivate_date")))
            {
                adherent.DeactivateDate = dr.GetDateTime(dr.GetOrdinal("deactivate_date"));
            }

            if (!dr.IsDBNull(dr.GetOrdinal("activation_code")))
            {
                adherent.ActivationCode = dr.GetInt32(dr.GetOrdinal("activation_code"));
            }

            return adherent;
        }

    }
}