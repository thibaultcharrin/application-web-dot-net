#!/bin/bash

#VARIABLES
IMAGE=thibaultcharrin/dld-store
TAG=latest
CONTAINER_REGISTRY=registry.gitlab.com/   # use empty string "" to use default container registry Docker Hub
KUBERNETES_VERSION=v1.24.6  # preferred version for stable display of minikube dashboard
KUBE_SECRET=""
MYSQL_ROOT_PASSWORD=$(grep MYSQL_ROOT_PASSWORD ../.env | cut -d "=" -f 2 | tr -d '\n' | base64 -w0)
MYSQL_DATABASE=$(grep MYSQL_DATABASE ../.env | cut -d "=" -f 2 | tr -d '\n' | base64 -w0)
MYSQL_USER=$(grep MYSQL_USER ../.env | cut -d "=" -f 2 | tr -d '\n' | base64 -w0)
MYSQL_PASSWORD=$(grep MYSQL_PASSWORD ../.env | cut -d "=" -f 2 | tr -d '\n' | base64 -w0)
# do not remove newline character for files
MYSQL_INIT=$(cat ../sql/1_initialize.sql | base64 -w0)
MYSQL_STRUCTURE=$(cat ../sql/2_structure.sql | base64 -w0)
MYSQL_DATA=$(cat ../sql/3_data.sql | base64 -w0)

#ARGS
if [[ $1 != "" ]]
then 
    TAG=$1
fi
if [[ $2 != "" ]]
then 
    CONTAINER_REGISTRY=$2
fi

#CONSTRUCT
DESTINATION=$CONTAINER_REGISTRY$IMAGE:$TAG

#STEPS
echo -e "\n*** Docker login check ***"
if [ -f ~/.docker/config.json ] 
then
    if [[ $CONTAINER_REGISTRY == "registry.gitlab.com/" ]] && [ $(grep registry.gitlab.com ~/.docker/config.json | wc -l) != 0 ]
    then    
        KUBE_SECRET=$(cat ~/.docker/config.json | base64 -w0)
    else
        echo "please enter docker login registry.gitlab.com command prior to launching pipeline"
        exit
    fi
else
    echo "please check your .docker/config.json "
    exit
fi

echo -e "\n*** Minikube init steps ***"
if [ $(minikube status | grep Running | wc -l) == 0 ]
then 
    minikube start --kubernetes-version $KUBERNETES_VERSION
fi
kubectl cluster-info

echo -e "\n*** Variable assignement ***"
cp 2-secrets.yaml 2-secrets-temp.yaml
# always use "|" sed delimiter as "/" is base64 character 
sed -i "s|\${KUBE_SECRET}|$KUBE_SECRET|g" 2-secrets-temp.yaml
sed -i "s|\${MYSQL_ROOT_PASSWORD}|$MYSQL_ROOT_PASSWORD|g" 2-secrets-temp.yaml
sed -i "s|\${MYSQL_DATABASE}|$MYSQL_DATABASE|g" 2-secrets-temp.yaml
sed -i "s|\${MYSQL_USER}|$MYSQL_USER|g" 2-secrets-temp.yaml
sed -i "s|\${MYSQL_PASSWORD}|$MYSQL_PASSWORD|g" 2-secrets-temp.yaml
sed -i "s|\${MYSQL_INIT}|$MYSQL_INIT|g" 2-secrets-temp.yaml
sed -i "s|\${MYSQL_STRUCTURE}|$MYSQL_STRUCTURE|g" 2-secrets-temp.yaml
sed -i "s|\${MYSQL_DATA}|$MYSQL_DATA|g" 2-secrets-temp.yaml
cp 5-app.yaml 5-app-temp.yaml && sed -i "s|\${DESTINATION}|$DESTINATION|g" 5-app-temp.yaml

echo -e "\n*** Applying Kubernetes descriptors ***"
kubectl apply -f 1-namespace.yaml
kubectl apply -f 2-secrets-temp.yaml
kubectl apply -f 3-volume.yaml
kubectl apply -f 4-mysql.yaml
while [ $(kubectl rollout status deploy mysql -n production | grep success | wc -l) == 0 ]    # kubectl rollout status awaits "successfully rolled out"
do
	sleep 1
done
kubectl apply -f 5-app-temp.yaml

echo -e "\n*** Final steps ***"
rm *temp.yaml
kubectl get namespaces
minikube tunnel
